== Posibles localizaciones

// Describir lugar y poner alguna foto. ¡Ojo!

=== Rivera del Huéznar

Bonito lugar en la Sierra Norte de Sevilla.

.Ribera del río Huéznar a su paso por La Fundición. https://commons.wikimedia.org/wiki/File:Ribera_del_Hueznar_01.jpg[Créditos].
image::images/576px-Ribera_del_Hueznar_01.jpg[Ribera del Huéznar]

=== Peñón del Cuervo, Málaga.

Zona de playa donde hay barbacoas construidas y es legal realizarlas.

.Peñón del Cuervo. https://www.tripadvisor.es/Attraction_Review-g187438-d14862738-Reviews-Playa_Penon_del_Cuervo-Malaga_Costa_del_Sol_Province_of_Malaga_Andalucia.html#photos;aggregationId=&albumid=101&filter=7&ff=375405619[Creditos]
image::images/penon_del_cuervo.jpg[Peñón del cuervo]
